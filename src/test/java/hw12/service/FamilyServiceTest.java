package hw12.service;

import hw12.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

class FamilyServiceTest {

    FamilyService service;
    Family familyEnglish;
    Family familyUS;
    Human fatherEnglish;
    Human motherEnglish;
    Human motherUS;
    Human fatherUS;
    Human childEnglishOldest;
    Human childUSMiddle;
    Human childUSJunior;
    List<Pet> pets;
    List<Family> families;
    Pet dog;

    @BeforeEach
    void creatingInstances() throws ParseException {
        service = new FamilyService();
        fatherEnglish = new Man("John", "Adams", 1987, 12, 2, 90);
        motherEnglish = new Woman("Ann", "Adams", 1989, 2, 2, 22);
        motherUS = new Woman("Sylwia", "Jackson", 1983, 3, 1, 98);
        fatherUS = new Man("Brian", "Jackson", 1984, 7, 12, 90);
        childEnglishOldest = new Man("Jack", "Adams", 2002, 2, 9, 99);
        childUSMiddle = new Man("Davis", "Jackson", 2003, 2, 9, 78);
        childUSJunior = new Man("Wade", "Jackson", 2004, 5, 2, 67);
        familyEnglish = new Family(motherEnglish, fatherEnglish);
        familyUS = new Family(motherUS, fatherUS);
        families = new ArrayList<>();
        dog = new Dog("Rex", 2);
        Pet fish = new Fish("Huraki", 1);
        Pet cat = new Cat("MurMur", 3);
        pets = new ArrayList<>();
        pets.add(dog);
        pets.add(fish);
        pets.add(cat);
    }

    @Test
    void displayAllFamilies() {
        Family englishFamily = service.createNewFamily(fatherEnglish, motherEnglish);
        Family usFamily = service.createNewFamily(fatherUS, motherUS);
        assertFalse(service.getAllFamilies().isEmpty());
        service.displayAllFamilies();
        service.deleteFamily(usFamily);
        service.deleteFamily(englishFamily);
        System.out.println(service.getAllFamilies());
    }

    @Test
    void getFamiliesBiggerThan() {
        Family englishFamily = service.createNewFamily(motherUS, fatherEnglish);
        Family USFamily = service.createNewFamily(motherEnglish, fatherUS);
        service.bornChild(USFamily, childUSMiddle);
        service.adoptChild(USFamily, childUSJunior);
        assertEquals(service.getFamiliesBiggerThan(2), 1);
    }

    @Test
    void getFamiliesLessThan() {
        Family englishFamily = service.createNewFamily(motherEnglish, fatherUS);
        Family USFamily = service.createNewFamily(motherUS, fatherEnglish);
        service.bornChild(USFamily, childEnglishOldest);
        service.adoptChild(USFamily, childUSMiddle);
        assertEquals(service.getFamiliesLessThan(3), 1);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        Family englishFamily = service.createNewFamily(fatherEnglish, motherEnglish);
        Family USFamily = service.createNewFamily(fatherUS, motherUS);
        service.bornChild(USFamily, childUSMiddle);
        service.adoptChild(USFamily, childUSJunior);
        assertEquals(service.countFamiliesWithMemberNumber(2), 1);
    }

    @Test
    void createNewFamily() {
        assertTrue(service.getFamilyById(service.createNewFamily(motherUS, fatherUS).getId()).isPresent());
    }

    @Test
    void bornChild() {
        assertNotNull(service.bornChild(familyEnglish, childUSMiddle));
    }

    @Test
    void adoptChild() {
        assertNotNull(service.adoptChild(familyEnglish, childUSMiddle));
    }

    @Test
    void deleteAllChildrenOlderThen() {
        Family USFamily = service.createNewFamily(fatherUS, motherUS);
        service.bornChild(USFamily, childUSMiddle);
        service.adoptChild(USFamily, childUSJunior);
        assertFalse(service.deleteAllChildrenOlderThen(18));
    }

    @Test
    void count() {
        Family englishFamily = service.createNewFamily(fatherEnglish, motherEnglish);
        Family USFamily = service.createNewFamily(fatherUS, motherUS);
        assertEquals(service.count(), 2);
    }

    @Test
    void getFamilyById() {
        Family englishFamily = service.createNewFamily(fatherEnglish, motherEnglish);
        Family USFamily = service.createNewFamily(fatherUS, motherUS);
        assertEquals(service.getFamilyById(englishFamily.getId()).get(), englishFamily);
    }

    @Test
    void getPets() {
        Family englishFamily = service.createNewFamily(fatherEnglish, motherEnglish);
        englishFamily.setPets(pets);
        assertFalse(service.getPets(0).isEmpty());
    }

    @Test
    void addPet() {
        Family englishFamily = service.createNewFamily(fatherEnglish, motherEnglish);
        assertFalse(service.addPet(englishFamily.getId(),dog).isEmpty());
    }

    @Test
    void getAllFamilies() {
        Family englishFamily = service.createNewFamily(fatherEnglish, motherEnglish);
        Family USFamily = service.createNewFamily(fatherUS, motherUS);
        service.bornChild(USFamily, childUSMiddle);
        service.adoptChild(USFamily, childUSJunior);
        assertFalse(service.getAllFamilies().isEmpty());
    }

    @Test
    void getFamilyByIndex() {
        Family USFamily = service.createNewFamily(fatherUS, motherUS);
        service.bornChild(USFamily, childUSMiddle);
        service.adoptChild(USFamily, childUSJunior);
        assertNotNull(service.getFamilyByIndex(USFamily.getId()));
    }

    @Test
    void deleteFamily() {
        Family USFamily = service.createNewFamily(fatherUS, motherUS);
        service.bornChild(USFamily, childUSMiddle);
        service.adoptChild(USFamily, childUSJunior);
        assertTrue(service.deleteFamily(USFamily));
    }

    @Test
    void saveFamily() {
        service.saveFamily(familyUS);
        assertFalse(service.getAllFamilies().isEmpty());
    }
}