package hw06;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    //creating instances for achieving them in all methods

    Family family;
    Human childOne;
    Human childTwo;
    Human childThree;
    Human childOfSona;
    Pet pet;

    @BeforeEach
    void creatingFamily(){
        Human mother = new Human("Elizabeth");
        Human father = new Human("John");
        pet = new Pet(Species.DOG,"Rex");
        family = new Family(mother,father);
        family.setPet(pet);
        childOne = new Human();
        childTwo = new Human();
        childThree = new Human();
        family.addChild(childOne);
        family.addChild(childTwo);
        family.addChild(childThree);
    }

    @Test
    void testToString() {
        assertSame(family.getFather().getName(),"John");
        assertSame(family.getMother().getName(),"Elizabeth");
    }

    @Test
    void deleteChild() {
        assertTrue(family.deleteChild(2));
    }

    @Test
    void countFamily() {
        assertEquals(family.countFamily(),5);
    }

    @Test
    void testEquals() {
        Human mother2 = new Human("Elizabeth");
        Human father2 = new Human("John");
        Pet pet2 = new Pet(Species.DOG,"Rex");
        Family family2 = new Family(mother2,father2);
        family2.setPet(pet2);
        Human childOne2 = new Human();
        Human childTwo2 = new Human();
        Human childThree2 = new Human();
        family2.addChild(childOne2);
        family2.addChild(childTwo2);
        family2.addChild(childThree2);

        assertEquals(family2,family);
    }

    @Test
    void testDeleteChild() {
        assertTrue(family.deleteChild(childOne));
    }

    //testing is deleteChild method working for Sona's child (another family)
    @Test
    void testDeleteChild1() {
        assertFalse(family.deleteChild(childOfSona));
    }
}