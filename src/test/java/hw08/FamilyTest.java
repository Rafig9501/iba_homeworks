package hw08;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FamilyTest {

    Family family;

    @BeforeEach
    void creatingFamily(){

        Human mother = new Human("Elizabeth");
        Human father = new Human("John");
        family = new Family(mother,father);
        Human childOne = new Human();
        Human childTwo = new Human();
        Human childThree = new Human();
        family.addChild(childOne);
        family.addChild(childTwo);
        family.addChild(childThree);
    }

    @Test
    void deleteChild() {
        assertTrue(family.deleteChild(2));
    }

    @Test
    void countFamily() {
        assertEquals(family.countFamily(),5);
    }
}