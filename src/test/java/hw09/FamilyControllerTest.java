package hw09;

import hw09.controller.FamilyController;
import hw09.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

class FamilyControllerTest {

    FamilyController controller;
    Family familyEnglish;
    Family familyUS;
    Human fatherEnglish;
    Human motherEnglish;
    Human motherUS;
    Human fatherUS;
    Human childEnglishOldest;
    Human childEnglishMiddle;
    Human childEnglishJunior;
    Human childUSOldest;
    Human childUSMiddle;
    Human childUSJunior;
    List<Pet> pets;
    List<Family> families;

    @BeforeEach
    void creatingInstances() {
        controller = new FamilyController();
        fatherEnglish = new Man("John", "Adams", 1987);
        motherEnglish = new Woman("Ann", "Adams", 1989);
        motherUS = new Woman("Sylwia", "Jackson", 1983);
        fatherUS = new Man("Brian", "Jackson", 1984);
        childEnglishOldest = new Man("Jack", "Adams", 2002);
        childEnglishMiddle = new Woman("Celena");
        childEnglishJunior = new Woman();
        childUSOldest = new Man();
        childUSMiddle = new Man("Davis", "Jackson", 2003);
        childUSJunior = new Man("Wade", "Jackson", 2004);
        familyEnglish = new Family(motherEnglish, fatherEnglish);
        familyUS = new Family(motherUS, fatherUS);
        families = new ArrayList<>();
    }

    @Test
    void displayAllFamilies() {
        createNewUSFamily();
        createNewEnglishFamily();
        controller.displayAllFamilies();
    }

    @Test
    void getFamiliesBiggerThan() {
        createNewEnglishFamily();
        createNewUSFamily();
        assertEquals(controller.getFamiliesBiggerThan(1), 2);
    }

    @Test
    void getFamiliesLessThan() {
        createNewEnglishFamily();
        createNewUSFamily();
        assertEquals(controller.getFamiliesLessThan(1), 0);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        createNewEnglishFamily();
        createNewUSFamily();
        assertEquals(controller.countFamiliesWithMemberNumber(2), 2);
    }

    @Test
    void createNewEnglishFamily() {
        assertEquals(familyEnglish, controller.createNewFamily(motherEnglish, fatherEnglish));
    }

    @Test
    void createNewUSFamily() {
        assertEquals(controller.createNewFamily(motherUS, fatherUS), familyUS);
    }

    @Test
    void bornChild() {
//        testing has child added and whether woman or man depending on which class object we passed for child
        System.out.println((controller.bornChild(familyEnglish, childEnglishJunior, "Agamirze", "Seyyidnise"))
                .getChildrens());
    }

    @Test
    void adoptChild() {
        controller.adoptChild(familyEnglish, childEnglishJunior);
        assertEquals(familyEnglish.getChildrens().size(), 1);
    }

    @Test
    void deleteAllChildrenOlderThan() {
        families = controller.deleteAllChildrenOlderThen(18);
    }

    @Test
    void count() {
        createNewEnglishFamily();
        createNewUSFamily();
        assertEquals(controller.count(), 2);
    }

    @Test
    void getFamilyById() {
        createNewEnglishFamily();
        createNewUSFamily();
        assertEquals(controller.getFamilyById(1), familyUS);
    }

    @Test
    void getPets() {
        addPet();
        assertEquals(controller.getPets(0), pets);
    }

    @Test
    void addPet() {
        createNewUSFamily();
        Pet dog = new Dog("Rex", 3);
        Pet cat = new DomesticCat("MurMur", 3);
        pets = new ArrayList<>();
        pets.add(dog);
        pets.add(cat);
        controller.addPet(0, dog);
        controller.addPet(0, cat);
    }

    @Test
    void getAllFamilies() {
        createNewUSFamily();
        createNewEnglishFamily();
        families.add(familyUS);
        families.add(familyEnglish);
        assertEquals(controller.getAllFamilies(), families);
    }

    @Test
    void getFamilyByIndex() {
        createNewEnglishFamily();
        assertEquals(controller.getFamilyById(0), familyEnglish);
    }

    @Test
    void deleteFamilyByIndex() {
        createNewEnglishFamily();
        createNewUSFamily();
        assertTrue(controller.deleteFamily(1));
        assertFalse(controller.deleteFamily(1));
    }

    @Test
    void deleteFamily() {
        createNewUSFamily();
        createNewEnglishFamily();
        assertTrue(controller.deleteFamily(familyUS));
    }

    @Test
    void saveFamily() {
        controller.saveFamily(familyEnglish);
    }
}