package hw05;

public class Main {

    public static void main(String[] args) {

        creatingFamilyOne();
        creatingFamilyTwo();
    }

    static void creatingFamilyOne() {

        System.out.println();

        Human mother = new Human("Elizabeth", "Johnson", 1996, 64, Human.addingActivities());
        Human father = new Human("Dustin", "Johnson", 1995, 68, Human.addingActivities());
        Human childOne = new Human("Diana", "Johnson", 2019, 47, Human.addingActivities());
        Human childTwo = new Human("Rihanna", "Johnson", 2018, 55, Human.addingActivities());
        Human childThree = new Human("Ziqmund", "Johnson", 2020, 37, Human.addingActivities());

        Pet pet = new Pet();
        pet.setSpecies("cat");
        pet.setNickname("Mastan");
        pet.setAge(1);
        String[] petHabits = {"run", "eat", "jump","destroy"};
        showingInfoFamily(mother, father, childOne, childTwo, childThree, pet, petHabits);
    }

    static void creatingFamilyTwo() {

        System.out.println();

        Human mother = new Human("Ann", "Adams", 1990, 84, Human.addingActivities());
        Human father = new Human("Eduard", "Adams", 1989, 78, Human.addingActivities());
        Human childOne = new Human("Tom", "Adams", 2019, 47, Human.addingActivities());
        Human childTwo = new Human("Andres", "Adams", 2018, 55, Human.addingActivities());
        Human childThree = new Human("Brain", "Adams", 2020, 37, Human.addingActivities());

        Pet fish = new Pet();
        fish.setSpecies("chinchilla");
        fish.setNickname("Puti");
        fish.setAge(2);
        String[] petHabits = {"run", "eat", "jump"};
        showingInfoFamily(mother, father, childOne, childTwo, childThree, fish, petHabits);
    }

    private static void showingInfoFamily(Human mother, Human father, Human childOne, Human childTwo, Human childThree, Pet fish, String[] petHabits) {
        fish.setHabits(petHabits);

        Family family = new Family(mother, father);

        family.addChild(childOne);
        family.addChild(childTwo);
        family.addChild(childThree);

        family.deleteChild(1);

        family.setPet(fish);

        System.out.println(family.toString());

        System.out.println();
    }
}
