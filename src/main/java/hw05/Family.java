package hw05;

import java.util.Arrays;
import java.util.Objects;

class Family {

    private Human mother;
    private Human father;
    private Human[] childrens;
    private Pet pet;
    private static int childrenAmount;
    private static int indexOfChild;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] childrens) {
        this.childrens = childrens;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Pet getPet() {
        return pet;
    }

    public Human[] getChildrens() {
        return childrens;
    }

    void addChild(Human child) {

        childrenAmount++;

        if (childrens == null) {
            Human[] childrens = new Human[childrenAmount];
            setChildren(childrens);
            childrens[indexOfChild] = child;
            indexOfChild++;
        } else {
            childrens = Arrays.copyOf(childrens, childrenAmount);
            childrens[indexOfChild] = child;
        }
    }

    boolean deleteChild(int indexChild) {

        if (indexChild > 0 && childrens != null && indexChild > childrens.length) {
            Human[] copyOfFamily = new Human[childrenAmount - 1];
            for (int i = 0, k = 0; i < childrens.length; i++) {
                if (i == indexChild) {
                    continue;
                }
                copyOfFamily[k++] = childrens[i];
            }
            childrens = Arrays.copyOf(copyOfFamily, copyOfFamily.length);
        }
        return true;
    }

    int countFamily() {
        return childrenAmount + 2;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(childrens) +
                ", pet=" + pet +
                '}' + " Family count is: " + countFamily();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(childrens, family.childrens) &&
                Objects.equals(pet.getNickname(), family.pet.getNickname()) &&
                Objects.equals(pet.getSpecies(), family.pet.getSpecies()) &&
                Objects.equals(pet.getAge(), family.pet.getAge());
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, childrens, pet);
    }
}