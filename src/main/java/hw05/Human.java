package hw05;

import java.util.Arrays;

class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname());
    }

    void describePet() {
        System.out.println("I have a " + family.getPet().getSpecies() + " he is " + family.getPet().getAge() + " years old" +
                " he is " + isPetTricky());
    }

    String isPetTricky() {

        String tricky;

        tricky = Pet.isTricky(family.getPet().getTrickLevel()) ? " he is very sly" : " he is almost not sly";

        return tricky;
    }


    static String [][] addingActivities (){

        String [][] schedule = new String[2][2];

        schedule[0][0] = "Friday";
        schedule[0][1] = "Roast marshmallows over a fire and make s’mores";

        schedule[1][0] = "Sunday";
        schedule[1][1] = "go to visit parents";

        return schedule;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}