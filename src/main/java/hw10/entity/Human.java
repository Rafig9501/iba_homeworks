package hw10.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String, String> schedule;

    public Human(String name, String surname, int yyyy, int MM, int dd) {
        this.name = name;
        this.surname = surname;
        String sb = dd+"/"+MM+"/"+yyyy;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date;
        try {
            date = dateFormat.parse(sb);
            birthDate = date.getTime();
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Human(String name) {
        this.name = name;
    }

    public Human() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getIq() {
        return iq;
    }

    public int getAgeInYears(){
        LocalDate localBirthDate =
                Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDateNow = LocalDate.now();
        Period age = Period.between(localBirthDate,localDateNow);
        return age.getYears();
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public String describeAge() {
        LocalDate localBirthDate =
                Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDateNow = LocalDate.now();
        Period age = Period.between(localBirthDate,localDateNow);
        return age.getYears() + " years " + age.getMonths() + " months " + age.getDays() + " days ";
    }

    public String age(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(birthDate);
        return dateFormat.format(date);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + age() +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, schedule);
    }
}