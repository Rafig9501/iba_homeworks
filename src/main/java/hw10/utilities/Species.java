package hw10.utilities;

public enum Species {

    DOG, CAT, FISH, UNKNOWN;
}
