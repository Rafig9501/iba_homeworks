package hw10.dao;

import hw10.entity.Family;
import hw10.entity.Human;
import hw10.entity.Pet;

import java.util.List;

public interface CollectionFamilyDao extends FamilyDAO {

    void displayAllFamilies();

    int getFamiliesBiggerThan(int peopleAmount);

    int getFamiliesLessThan(int peopleAmount);

    int countFamiliesWithMemberNumber(int peopleAmount);

    Family createNewFamily(Human mother, Human father);

    Family bornChild(Family family, Human human, String manName, String womanName);

    Family adoptChild(Family family, Human human);

    List<Family> deleteAllChildrenOlderThen(int age);

    int count();

    Family getFamilyById(int index);

    List<Pet> getPets(int familyIndex);

    List<Pet> addPet(int familyIndex, Pet pet);
}
