package hw02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Homework2 {

    static int targetRow;
    static int targetColumn;
    static String[][] square;
    static BufferedReader reader;

    public static void main(String[] args) throws IOException {

        reader = new BufferedReader(new InputStreamReader(System.in));

        square = new String[6][6];

        targetRow = generatingRandomNumber();

        targetColumn = generatingRandomNumber();

        System.out.println("All set. Get ready to rumble!");

        for (int i = 0; i < square.length; i++) {

            for (int j = 0; j < square[i].length; j++) {

                square[i][0] = String.valueOf(i);

                square[0][j] = String.valueOf(j);

                if (i != 0 & j != 0) {
                    square[i][j] = "-";
                }

                System.out.print(square[i][j] + " | ");
            }
            System.out.println();
        }

        guessNumber();
    }

    static int generatingRandomNumber() {

        double randomDouble = Math.random();

        return (int) (randomDouble * 5 + 1);
    }

    static void guessNumber() throws IOException {

        System.out.println("\nPlease enter row number");

        String rowNumber = reader.readLine();

        System.out.println("\nPlease enter column number");

        String columnNumber = reader.readLine();

        if (rowNumber != null && columnNumber != null && !rowNumber.isEmpty() && !columnNumber.isEmpty()) {

            for (int i = 0; i < rowNumber.length(); i++) {

                if (!Character.isDigit(rowNumber.charAt(i))) {

                    System.out.println("Please write a integer number between 1 and 5");

                    guessNumber();
                }
            }

            for (int i = 0; i < columnNumber.length(); i++) {

                if (!Character.isDigit(columnNumber.charAt(i))) {

                    System.out.println("Please write a integer number between 1 and 5");

                    guessNumber();
                }
            }

            if (0 <= Integer.parseInt(rowNumber) && Integer.parseInt(rowNumber) <= 5
                    && 0 <= Integer.parseInt(columnNumber) && Integer.parseInt(columnNumber) <= 5) {

                checkingTarget(Integer.parseInt(rowNumber), Integer.parseInt(columnNumber));

            } else {
                System.out.println("Please write number between 1 and 5");

                guessNumber();
            }

        } else {
            System.out.println("Please write number for raw and column \n");

            guessNumber();
        }
    }

    static void checkingTarget(int rowNumber, int columnNumber) throws IOException {

        if (rowNumber == targetRow && columnNumber == targetColumn) {

            for (int i = 0; i < square.length; i++) {

                for (int j = 0; j < square[i].length; j++) {

                    square[i][0] = String.valueOf(j);

                    square[rowNumber][columnNumber] = "X";

                    System.out.print(square[i][j] + " | ");
                }

                System.out.println();
            }

            System.out.println("You have won!");

        } else {

            for (int i = 0; i < square.length; i++) {

                for (int j = 0; j < square[i].length; j++) {

                    square[i][0] = String.valueOf(i);

                    square[0][j] = String.valueOf(j);

                    square[rowNumber][columnNumber] = "*";

                    System.out.print(square[i][j] + " | ");
                }

                System.out.println();
            }

            System.out.println("You missed the target \nTry again \n");

            guessNumber();

        }
    }
}