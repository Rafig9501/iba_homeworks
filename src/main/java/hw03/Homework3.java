package hw03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework3 {

    static String[][] schedule;

    static BufferedReader reader;

    public static void main(String[] args) throws IOException {

        reader = new BufferedReader(new InputStreamReader(System.in));

        schedule = new String[7][2];

        addingDaysOfWeek(schedule);

        checkingInput();
    }

    private static void addingDaysOfWeek(String[][] schedule) {

        schedule[0][0] = "Monday";
        schedule[0][1] = "Blow bubbles";

        schedule[1][0] = "Tuesday";
        schedule[1][1] = "Walk on the boardwalk";

        schedule[2][0] = "Wednesday";
        schedule[2][1] = "Catch fireflies at night";

        schedule[3][0] = "Thursday";
        schedule[3][1] = "Eat a whole lobster with your hands";

        schedule[4][0] = "Friday";
        schedule[4][1] = "Roast marshmallows over a fire and make s’mores";

        schedule[5][0] = "Saturday";
        schedule[5][1] = "Nap in a hammock";

        schedule[6][0] = "Sunday";
        schedule[6][1] = "go to visit parents";
    }

    private static void checkingInput() throws IOException {

        System.out.println("Please, input the day of the week:");
        System.out.println("If you want close the app please enter - \"exit\" ");
        System.out.println("If you want change activities please enter - \"change\" ");

        String dayOfWeek = reader.readLine();

        // Checking do we get any characters or numbers from input
        Pattern pattern = Pattern.compile("[\\W, \\d]");

        Matcher matcher = pattern.matcher(dayOfWeek);

        if (dayOfWeek.equalsIgnoreCase("exit")) {

            System.out.println("You closed the app, thanks");

        } else if (dayOfWeek.equalsIgnoreCase("change")) {

            System.out.println("Please write a day which you want to change : ");

            String changeActivity = reader.readLine();

            if (!matcher.find()) {

                for (String[] strings : schedule) {

                    if (strings[0].equalsIgnoreCase(changeActivity)) {

                        changingActivity(changeActivity);
                    }
                }
            }
        } else if (!matcher.find()) {

            showingActivities(dayOfWeek);

        } else {

            System.out.println("Sorry, I don't understand you, please try again.");

            checkingInput();
        }
    }


    private static void showingActivities(String dayOfWeek) throws IOException {

        switch (dayOfWeek.toLowerCase()) {

            case "monday":
                activitySearching(dayOfWeek, 0);
                break;

            case "tuesday":
                activitySearching(dayOfWeek, 1);
                break;

            case "wednesday":
                activitySearching(dayOfWeek, 2);
                break;

            case "thursday":
                activitySearching(dayOfWeek, 3);
                break;

            case "friday":
                activitySearching(dayOfWeek, 4);
                break;

            case "saturday":
                activitySearching(dayOfWeek, 5);
                break;

            case "sunday":
                activitySearching(dayOfWeek, 6);
                break;

            default:
                System.out.println("Sorry, I don't understand you, please try again.");
                checkingInput();
        }
    }

    private static void activitySearching(String dayOfWeek, int indexOfday) throws IOException {

        System.out.println("Your tasks for " + dayOfWeek + " : " + schedule[indexOfday][1]);

        System.out.println();

        continuingSearching();
    }

    private static void continuingSearching() throws IOException {

        System.out.println("Would you like to continue ? ");

        String answer = reader.readLine();

        if (answer.equalsIgnoreCase("yes")) {

            checkingInput();

        } else if (answer.equalsIgnoreCase("no")) {

            System.out.println("You closed the app, thanks!");

        } else {

            System.out.println("Please enter correct answer");

            continuingSearching();
        }
    }

    private static void changingActivity(String dayOfWeek) throws IOException {

        String newTask;

        switch (dayOfWeek.toLowerCase()) {

            case "monday":
                System.out.println("Please, input new tasks for " + schedule[0][0]);
                newTask = reader.readLine();
                schedule[0][1] = newTask;
                break;

            case "tuesday":
                System.out.println("Please, input new tasks for " + schedule[1][0]);
                newTask = reader.readLine();
                schedule[1][1] = newTask;
                break;

            case "wednesday":
                System.out.println("Please, input new tasks for " + schedule[2][0]);
                newTask = reader.readLine();
                schedule[2][1] = newTask;
                break;

            case "thursday":
                System.out.println("Please, input new tasks for " + schedule[3][0]);
                newTask = reader.readLine();
                schedule[3][1] = newTask;
                break;

            case "friday":
                System.out.println("Please, input new tasks for " + schedule[4][0]);
                newTask = reader.readLine();
                schedule[4][1] = newTask;
                break;

            case "saturday":
                System.out.println("Please, input new tasks for " + schedule[5][0]);
                newTask = reader.readLine();
                schedule[5][1] = newTask;
                break;

            case "sunday":
                System.out.println("Please, input new tasks for " + schedule[6][0]);
                newTask = reader.readLine();
                schedule[6][1] = newTask;
                break;

            default:
                System.out.println("Sorry, I don't understand you, please try again.");
                checkingInput();
        }

        System.out.println("Changes have been applied ");

        continuingSearching();
    }
}

