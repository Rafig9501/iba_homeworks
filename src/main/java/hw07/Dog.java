package hw07;

class Dog extends Pet implements Fouling {

    Species species;

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOG;
    }

    public Dog(String nickname, int age) {
        super(nickname, age);
        this.species = Species.DOG;
    }

    @Override
    public void foul() {
            System.out.println("'I need to cover it up'");
    }
}
