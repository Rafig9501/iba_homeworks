package hw07;

import java.util.Calendar;

public class Main {

    public static void main(String[] args) {

        Man man = new Man("John");
        Pet dog = new Dog("Rex",2);

        man.greetPet(dog);

        Woman woman = new Woman("Anna");
        Pet domesticCat = new DomesticCat("Mur-Mur",1);

        woman.greetPet(domesticCat);
    }
}
