package hw07;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.Objects;

class Family {

    private Human mother;
    private Human father;
    private Human[] childrens;
    private Pet pet;
    private static int childrenAmount;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family() {

    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] childrens) {
        this.childrens = childrens;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Pet getPet() {
        return pet;
    }

    public Human[] getChildrens() {
        return childrens;
    }

    boolean deleteChild(int indexChild) {

        if (childrens != null && indexChild < childrens.length) {
            childrens = ArrayUtils.remove(childrens, indexChild);
            return true;
        } else return false;
    }

    boolean deleteChild(Human expectedChild) {

        if (childrens != null) {
            if (ArrayUtils.contains(childrens, expectedChild)) {
                childrens = ArrayUtils.removeElement(childrens, expectedChild);
                return true;
            } else return false;
        } else return false;
    }

    int countFamily() {
        return childrenAmount + 2;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(childrens) +
                ", pet=" + pet +
                '}' + " Family count is: " + countFamily();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(childrens, family.childrens) &&
                Objects.equals(pet.getNickname(), family.pet.getNickname()) &&
                Objects.equals(pet.getAge(), family.pet.getAge());
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, childrens, pet);
    }

    void addChild(Human child) {
        if (childrens == null) {
            childrens = new Human[childrenAmount];
            childrens = ArrayUtils.add(childrens, child);
            childrenAmount++;
        } else {
            childrens = ArrayUtils.add(childrens, child);
            childrenAmount++;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Object " + this + " has been removed" );
    }
}
