package hw07;

import java.util.Arrays;

class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq, String[][] schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    public Human(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    String isPetTricky() {

        String tricky;

        tricky = Pet.isTricky(family.getPet().getTrickLevel()) ? " he is very sly" : " he is almost not sly";

        return tricky;
    }

    void greetPet(Pet pet) {

        System.out.println("Hello, " + pet.getNickname());
    }

    String[][] addingActivities() {

        if (schedule == null) {
            schedule = new String[2][2];

            schedule[0][0] = "Friday";
            schedule[0][1] = "Roast marshmallows over a fire and make s’mores";

            schedule[1][0] = "Sunday";
            schedule[1][1] = "go to visit parents";
        } else {
            schedule[0][0] = "Friday";
            schedule[0][1] = "Roast marshmallows over a fire and make s’mores";

            schedule[1][0] = "Sunday";
            schedule[1][1] = "go to visit parents";
        }
        return schedule;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object " + this + " deleted");
        super.finalize();
    }
}