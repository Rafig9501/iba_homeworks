package hw07;

public class Fish extends Pet {

    Species species;

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.FISH;
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
        this.species = Species.DOG;
    }
}
