package hw01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Homework1 {

    public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    static String name;
    static int randomNumber;

    public static void main(String[] args) throws IOException {

        System.out.println("Please write your name");

        name = reader.readLine();

        System.out.println("Let the game begin!");

        System.out.println("Enter number");

        randomNumber = (int) (Math.random()*100);

        checkingNumber();
    }

    static void checkingNumber () throws IOException {

        String input = reader.readLine();

        for (int i = 0;i < input.length();i++)
        {
            if (!Character.isDigit(input.charAt(i)))
            {
                System.out.println("Please write a integer number between 1 and 100");

                checkingNumber();
            }
        }

        if (Integer.parseInt(input) < randomNumber)
        {
            System.out.println("Your number is too small. Please, try again.");

            checkingNumber();
        }

        else if (Integer.parseInt(input) > randomNumber)
        {
            System.out.println("Your number is too big. Please, try again.");

            checkingNumber();
        }

        else if (Integer.parseInt(input) == randomNumber)
        {
            System.out.println("Congratulations, "+name);
        }
    }
}
