package hw04;

public class Human {

    String name;
    String surname;
    int year;
    int iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

    public Human() {
    }

    protected Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    void greetPet() {
        System.out.println("Hello, " + pet.nickname);
    }

    void describePet() {
        System.out.println("I have a " + pet.species + " he is " + pet.age + " years old" +
                " he is " + isPetTricky());
    }

    String isPetTricky() {

        String tricky;

        tricky = Pet.isTricky(pet.trickLevel) ? " he is very sly" : " he is almost not sly";

        return tricky;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", mother=" + mother.name + " " + mother.surname +
                ", father=" + father.name + " " + father.surname +
                ", pet=" + pet.toString() + '}';
    }

    static String [][] addingActivities (){

        String [][] schedule = new String[2][2];

        schedule[0][0] = "Friday";
        schedule[0][1] = "Roast marshmallows over a fire and make s’mores";

        schedule[1][0] = "Sunday";
        schedule[1][1] = "go to visit parents";

        return schedule;
    }
}
