package hw04;

public class Main {

    public static void main(String[] args) {

        creatingChineseFamily();

        creatingRussianFamily();

    }

    private static void showingInfoFamily(Pet pet, Human human) {
        System.out.println(human.toString());
        human.isPetTricky();
        human.describePet();
        human.greetPet();

        System.out.println(pet.toString());
        pet.eat();
        pet.foul();
        pet.respond();
    }

    private static void creatingChineseFamily(){

        String [] petChineseHabits = {"Eat","Sleep","Run"};

        Human motherChinese = new Human("Hiwata","Syi",1989);
        Human fatherChinese = new Human("Akita","Syi",1987);
        Pet petChinese = new Pet("dog","Rex",2,48,petChineseHabits);

        Human childChinese = new Human("Kyong","Syi",1999,86, petChinese,motherChinese,fatherChinese, Human.addingActivities());

        showingInfoFamily(petChinese, childChinese);
    }

    private static void creatingRussianFamily(){

        String [] petRussianHabits = {"Eat","Sleep","Run"};

        Human motherRussian = new Human("Валентина","Путин",1978);
        Human fatherRussian = new Human("Владимир","Путин",1982);
        Pet petRussian = new Pet("Кот","МурМур",2,87,petRussianHabits);

        Human childRussian = new Human("Саня","Путин",2000,58, petRussian,motherRussian,fatherRussian, Human.addingActivities());

        showingInfoFamily(petRussian, childRussian);
    }
}
