package hw06;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet() {
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.trickLevel = new Random().nextInt(100) + 1;
    }

    public Pet(Species species, String nickname, int age, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = new Random().nextInt(100) + 1;
        this.habits = habits;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    static boolean isTricky(int trickLevel){

        return trickLevel > 50;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Pet pet5 = (Pet) object;
        return age == pet5.age &&
                trickLevel == pet5.trickLevel &&
                Objects.equals(species, pet5.species) &&
                Objects.equals(nickname, pet5.nickname);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

    @Override
    public String toString() {
        return species + "{"  + "nickname=" + '\'' + nickname + '\'' + ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) + '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object " + this + " has been removed");
        super.finalize();
    }
}
