package hw13.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static hw12.console.ConsoleOUT.wrongCommand;
import static hw12.utilities.Constants_.*;

public class ConsoleIN {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String reader() throws IOException {
            return reader.readLine();
    }

    public static String childMenu() throws IOException {

        switch (reader()){
            case GIVE_BIRTH_TO_BABY:
                return GIVE_BIRTH_TO_BABY;
            case ADOPT_CHILD:
                return ADOPT_CHILD;
            case RETURN_TO_MAIN_MENU:
                return RETURN_TO_MAIN_MENU;
            default:
                wrongCommand();
                childMenu();
                return "";
        }
    }
}
