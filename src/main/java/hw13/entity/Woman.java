package hw13.entity;

import java.io.Serializable;
import java.text.ParseException;

public final class Woman extends Human implements Serializable {

    public Woman(String name, String surname, int yyyy, int MM, int dd,int IQ) throws ParseException {
        super(name, surname, yyyy, MM, dd, IQ);
    }

    public Woman(String name) {
        super(name);
    }

    public Woman() {
        super();
    }

}
