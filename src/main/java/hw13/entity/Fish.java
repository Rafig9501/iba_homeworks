package hw13.entity;

import hw13.utilities.Species;

import java.io.Serializable;

public class Fish extends Pet implements Serializable {

    Species species;

    public Fish(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
        this.species = Species.FISH;
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
        this.species = Species.FISH;
    }
}
