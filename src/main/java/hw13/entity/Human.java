package hw13.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human implements Serializable {

    private String name;
    private String surname;
    private long birthDate;
    private int IQ;
    private Map<String, String> schedule;

    public Human(String name, String surname, int yyyy, int MM, int dd, int IQ) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.IQ = IQ;
        String sb = dd + "/" + MM + "/" + yyyy;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date;
        date = dateFormat.parse(sb);
        birthDate = date.getTime();

    }

    public Human(String name) {
        this.name = name;
    }

    public Human() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getIQ() {
        return IQ;
    }

    public int getAgeInYears() {
        LocalDate localBirthDate =
                Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDateNow = LocalDate.now();
        Period age = Period.between(localBirthDate, localDateNow);
        return age.getYears();
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public String describeAge() {
        LocalDate localBirthDate =
                Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDateNow = LocalDate.now();
        Period age = Period.between(localBirthDate, localDateNow);
        return age.getYears() + " years " + age.getMonths() + " months " + age.getDays() + " days ";
    }

    public String age() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(birthDate);
        return dateFormat.format(date);
    }

    public String prettyFormat(){
        return "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + age() +
                ", iq=" + IQ +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public String toString() {
        return prettyFormat();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                IQ == human.IQ &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, IQ, schedule);
    }
}