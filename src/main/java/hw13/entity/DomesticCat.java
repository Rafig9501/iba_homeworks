package hw13.entity;

import hw13.utilities.Species;

import java.io.Serializable;

public class DomesticCat extends Pet implements Serializable {

    Species species;

    public DomesticCat(String nickname, int age,String[] habits) {
        super(nickname, age, habits);
        this.species = Species.CAT;
    }

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
        this.species = Species.DOG;
    }
}
