package hw13.entity;

import java.io.Serializable;

public class Cat extends Pet implements Serializable {

    public Cat(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
    }

    public Cat(String nickname, int age) {
        super(nickname, age);
    }
}
