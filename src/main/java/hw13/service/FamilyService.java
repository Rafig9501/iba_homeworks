package hw13.service;

import hw13.console.ConsoleOUT;
import hw13.dao.CollectionFamilyDao;
import hw13.dao.FamilyDAO;
import hw13.entity.Family;
import hw13.entity.Human;
import hw13.entity.Pet;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FamilyService implements CollectionFamilyDao {

    static FamilyDAO familyService;

    public final static File file = new File("families.bin");

    public FamilyService() {
        familyService = this;
    }

    @Override
    public void displayAllFamilies() {
        ConsoleOUT.print(getAllFamilies().toString());
    }

    @Override
    public int getFamiliesBiggerThan(int peopleAmount) {
        return (int) getAllFamilies().stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) > peopleAmount).count();
    }

    @Override
    public int getFamiliesLessThan(int peopleAmount) {
        return (int) getAllFamilies().stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) < peopleAmount).count();
    }

    @Override
    public int countFamiliesWithMemberNumber(int peopleAmount) {
        return (int) getAllFamilies().stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) == peopleAmount).count();
    }

    @Override
    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        List<Family> families = getAllFamilies();
        families.add(family);
        loadData(families);
        return family;
    }

    @Override
    public Family bornChild(Family family, Human human) {
        if (family.getChildrens() != null) {
            family.getChildrens().add(human);
        }
        if (family.getChildrens() == null) {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(human);
        }
        return family;
    }

    @Override
    public Family adoptChild(Family family, Human human) {
        if (family.getChildrens() != null) {
            family.getChildrens().add(human);
        }
        if (family.getChildrens() == null) {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(human);
        }
        return family;
    }

    @Override
    public boolean deleteAllChildrenOlderThen(int age) {
        return getAllFamilies().removeIf(family -> family.getChildrens().stream()
                .anyMatch(human -> human.getAgeInYears() > age));
    }

    @Override
    public int count() {
        return getAllFamilies().size();
    }

    @Override
    public Optional<Family> getFamilyById(int index) {
        return getAllFamilies().stream().filter(family -> family.getId() == index).findFirst();
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        return getAllFamilies().get(familyIndex).getPets();
    }

    @Override
    public List<Pet> addPet(int familyIndex, Pet pet) {
        if (getAllFamilies() == null || !(getFamilyById(familyIndex).isPresent())) {
            return null;
        } else if (getFamilyById(familyIndex).get().getPets() == null) {
            getFamilyById(familyIndex).get().setPets(new ArrayList<>());
            getFamilyById(familyIndex).get().getPets().add(pet);
            return getFamilyById(familyIndex).get().getPets();
        } else {
            getFamilyById(familyIndex).get().getPets().add(pet);
            return getFamilyById(familyIndex).get().getPets();
        }
    }

    @Override
    public List<Family> getAllFamilies() {
        try {
            if (file.exists()) {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
                List<Family> families = (List<Family>) ois.readObject();
                ois.close();
                return families;
            }
            else {
                List<Family> families = new ArrayList<>();
                loadData(families);
                return families;
            }
        } catch (Exception e) {
            List<Family> families = new ArrayList<>();
            return families;
        }
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return getFamilyById(index).get();
    }

    @Override
    public boolean deleteFamily(int index) {
        if (getFamilyById(index).isPresent()) {
            List<Family> families = getAllFamilies();
            boolean removed = families.remove(getFamilyById(index).get());
            loadData(families);
            return removed;
        } else return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (getAllFamilies().contains(family)) {
            List<Family> families = getAllFamilies();
            boolean removed = families.remove(family);
            loadData(families);
            return removed;
        } else return false;
    }

    @Override
    public void saveFamily(Family family) {
        getAllFamilies().add(family);
    }

    public boolean loadData(List<Family> families) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(families);
            oos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
