package hw13.utilities;

public class Constants_ {

    final public static String CREATE_FAMILY = "1";
    final public static String DELETE_FAMILY = "2";
    final public static String EDIT_FAMILY = "3";
    final public static String GET_ALL_FAMILIES = "4";
    final public static String FAMILIES_WITH_COUNT_OF_PEOPLE_LESS_THAN = "5";
    final public static String FAMILIES_WITH_COUNT_OF_PEOPLE_MORE_THAN = "6";
    final public static String FAMILIES_WITH_COUNT_OF_PEOPLE_EQUALS_TO = "7";
    final public static String REMOVE_ALL_CHILDREN_OLDER_THAN = "8";
    final public static String EXIT = "9";

    final public static String GIVE_BIRTH_TO_BABY = "1";
    final public static String ADOPT_CHILD = "2";
    final public static String ADD_PETS = "3";
    final public static String RETURN_TO_MAIN_MENU = "4";

    final public static String YES = "1";
    final public static String NO = "2";

    final public static String DOG = "1";
    final public static String FISH = "2";
    final public static String CAT = "3";
}
