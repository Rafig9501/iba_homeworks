package hw13.utilities;

public class FamilyOverflowException extends RuntimeException {

    public FamilyOverflowException(String message){
        super(message);
    }
}
