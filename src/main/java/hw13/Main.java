package hw13;

import hw13.controller.Controller;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        Controller controller = new Controller();
        controller.mainMenu();
    }
}
