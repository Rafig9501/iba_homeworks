package hw08;

import java.util.HashMap;
import java.util.Map;

class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<String,String> schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq, Map<String,String> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    public Human(String name) {
        this.name = name;
    }

    public Human() {

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Map<String,String> getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    void greetPet(Pet pet){

        System.out.println("Hello, " + pet.getNickname());
    }

    Map<String,String> addingActivities (){

        if (schedule == null){
            schedule = new HashMap<>();
            schedule.put("Friday", "Roast marshmallows over a fire and make s’mores");
            schedule.put("Sunday", "go to visit parents");
        }
        else {
            schedule.put("Friday", "Roast marshmallows over a fire and make s’mores");
            schedule.put("Sunday", "go to visit parents");
        }
        return schedule;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule.toString() +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object " + this + " deleted");
        super.finalize();
    }
}