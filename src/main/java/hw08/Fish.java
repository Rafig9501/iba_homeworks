package hw08;

public class Fish extends Pet {

    Species species;

    public Fish(String nickname, int age, String[] habits, Species species) {
        super(nickname, age, habits);
        this.species = species;
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
        this.species = Species.DOG;
    }
}
