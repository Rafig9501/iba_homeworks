package hw08;

import java.util.Map;

final class Woman extends Human {

    public Woman(String name, String surname, int year, int iq, Map<String,String> schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }

    public Woman(String name) {
        super(name);
    }

    @Override
    void greetPet(Pet pet) {
        super.greetPet(pet);
    }

    void makeup(){

        System.out.println("I am doing makeup");
    }
}
