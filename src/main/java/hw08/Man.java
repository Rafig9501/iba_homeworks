package hw08;

import java.util.Map;

final class Man extends Human{

    public Man(String name, String surname, int year, int iq, Map<String,String> schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }

    public Man(String name) {
        super(name);
    }

    @Override
    void greetPet(Pet pet) {
        super.greetPet(pet);
    }

    void repairCar(){

        System.out.println("I am repairing my car");
    }
}
