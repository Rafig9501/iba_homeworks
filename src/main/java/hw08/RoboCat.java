package hw08;

public class RoboCat extends Pet {

    public RoboCat(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
    }

    public RoboCat(String nickname, int age) {
        super(nickname, age);
    }
}
