package hw08;

public class DomesticCat extends Pet implements Fouling {

    Species species;

    public DomesticCat(String nickname, int age, String[] habits, Species species) {
        super(nickname, age, habits);
        this.species = species;
    }

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
        this.species = Species.DOG;
    }

    @Override
    public void foul() {
        System.out.println("'I need to cover it up'");
    }
}
