package hw08;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class Family {

    private Human mother;
    private Human father;
    private List<Human> childrens;
    private List<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family() {

    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(List<Human> childrens) {
        this.childrens = childrens;
    }

    public void setPets(List<Pet> pets) {
        this.pets = pets;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public List<Human> getChildrens() {
        return childrens;
    }

    void addChild(Human child) {

        if (childrens==null) {
            childrens = new ArrayList<>();
            childrens.add(child);
        }
        else childrens.add(child);
    }

    boolean deleteChild(int indexChild) {

        if (indexChild > 0 && childrens != null && indexChild < childrens.size()) {
            childrens.remove(indexChild);
            return true;
            }
        else return false;
    }

    int countFamily(){
        return childrens.size()+2;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", childrens=" + childrens +
                ", pet=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(childrens, family.childrens) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, childrens, pets);
    }
}
