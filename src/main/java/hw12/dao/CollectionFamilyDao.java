package hw12.dao;

import hw12.entity.Family;
import hw12.entity.Human;
import hw12.entity.Pet;

import java.util.List;
import java.util.Optional;

public interface CollectionFamilyDao extends FamilyDAO {

    void displayAllFamilies();

    int getFamiliesBiggerThan(int peopleAmount);

    int getFamiliesLessThan(int peopleAmount);

    int countFamiliesWithMemberNumber(int peopleAmount);

    Family createNewFamily(Human mother, Human father);

    Family bornChild(Family family, Human human);

    Family adoptChild(Family family, Human human);

    boolean deleteAllChildrenOlderThen(int age);

    int count();

    Optional<Family> getFamilyById(int index);

    List<Pet> getPets(int familyIndex);

    List<Pet> addPet(int familyIndex, Pet pet);
}
