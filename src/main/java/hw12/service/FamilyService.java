package hw12.service;

import hw12.console.ConsoleOUT;
import hw12.dao.CollectionFamilyDao;
import hw12.dao.FamilyDAO;
import hw12.entity.Family;
import hw12.entity.Human;
import hw12.entity.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FamilyService implements CollectionFamilyDao {

    static FamilyDAO familyService;
    static List<Family> families;

    public FamilyService() {
        familyService = this;
        if (families == null) {
            families = new ArrayList<>();
        }
    }

    @Override
    public void displayAllFamilies() {
        ConsoleOUT.print(getAllFamilies().toString());
    }

    @Override
    public int getFamiliesBiggerThan(int peopleAmount) {
        return (int) getAllFamilies().stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) > peopleAmount).count();
    }

    @Override
    public int getFamiliesLessThan(int peopleAmount) {
        return (int) getAllFamilies().stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) < peopleAmount).count();
    }

    @Override
    public int countFamiliesWithMemberNumber(int peopleAmount) {
        return (int) getAllFamilies().stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) == peopleAmount).count();
    }

    @Override
    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        getAllFamilies().add(family);
        return family;
    }

    @Override
    public Family bornChild(Family family, Human human) {
        if (family.getChildrens() != null) {
            family.getChildrens().add(human);
        }
        if (family.getChildrens() == null) {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(human);
        }
        return family;
    }

    @Override
    public Family adoptChild(Family family, Human human) {
        if (family.getChildrens() != null) {
            family.getChildrens().add(human);
        }
        if (family.getChildrens() == null) {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(human);
        }
        return family;
    }

    @Override
    public boolean deleteAllChildrenOlderThen(int age) {
        return getAllFamilies().removeIf(family -> family.getChildrens().stream()
                .anyMatch(human -> human.getAgeInYears() > age));
    }

    @Override
    public int count() {
        return getAllFamilies().size();
    }

    @Override
    public Optional<Family> getFamilyById(int index) {
        return getAllFamilies().stream().filter(family -> family.getId() == index).findFirst();
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        return getAllFamilies().get(familyIndex).getPets();
    }

    @Override
    public List<Pet> addPet(int familyIndex, Pet pet) {
        if (families == null || !(getFamilyById(familyIndex).isPresent())) {
            return null;
        } else if (getFamilyById(familyIndex).get().getPets() == null) {
            getFamilyById(familyIndex).get().setPets(new ArrayList<>());
            getFamilyById(familyIndex).get().getPets().add(pet);
            return getFamilyById(familyIndex).get().getPets();
        } else {
            getFamilyById(familyIndex).get().getPets().add(pet);
            return getFamilyById(familyIndex).get().getPets();
        }
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return getFamilyById(index).get();
    }

    @Override
    public boolean deleteFamily(int index) {
        if (getFamilyById(index).isPresent()){
            return families.remove(getFamilyById(index).get());
        }else return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (families.contains(family)) {
            families.remove(family);
            return true;
        } else return false;
    }

    @Override
    public void saveFamily(Family family) {
        families.add(family);
    }
}
