package hw12.controller;

import hw12.console.ConsoleIN;
import hw12.console.ConsoleOUT;
import hw12.entity.*;
import hw12.utilities.FamilyOverflowException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static hw12.utilities.Constants_.*;

public class Controller {

    FamilyController controller;
    static Map<String, String> schedule;

    public Controller() {
        controller = new FamilyController();
    }

    public void mainMenu() throws IOException {
        ConsoleOUT.displayMainMenu();
        String command = ConsoleIN.reader();
        switch (command) {
            case CREATE_FAMILY:
                createFamily();
                mainMenu();
                break;
            case DELETE_FAMILY:
                deleteFamily();
                break;
            case GET_ALL_FAMILIES:
                ConsoleOUT.print(String.valueOf(controller.getAllFamilies()));
                mainMenu();
                break;
            case EDIT_FAMILY:
                ConsoleOUT.familyIDX();
                int index = 0;
                try {
                    index = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    mainMenu();
                }
                boolean present = controller.getFamilyById(index).isPresent();
                if (present) editFamily(index);
                else {
                    ConsoleOUT.wrongCommand();
                    mainMenu();
                }
                break;
            case FAMILIES_WITH_COUNT_OF_PEOPLE_LESS_THAN:
                ConsoleOUT.number();
                int count1 = 0;
                try {
                    count1 = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    mainMenu();
                }
                int familiesLessThan = controller.getFamiliesLessThan(count1);
                ConsoleOUT.print("Amount of families less than " + count1 + " is " + familiesLessThan);
                mainMenu();
                break;
            case FAMILIES_WITH_COUNT_OF_PEOPLE_EQUALS_TO:
                ConsoleOUT.number();
                int count2 = 0;
                try {
                    count2 = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    mainMenu();
                }
                int familiesEqualsTo = controller.countFamiliesWithMemberNumber(count2);
                ConsoleOUT.print("Amount of families less than " + count2 + " is " + familiesEqualsTo);
                mainMenu();
                break;
            case FAMILIES_WITH_COUNT_OF_PEOPLE_MORE_THAN:
                ConsoleOUT.number();
                int count3 = 0;
                try {
                    count3 = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    mainMenu();
                }
                int familiesMoreThan = controller.getFamiliesBiggerThan(count3);
                ConsoleOUT.print("Amount of families less than " + count3 + " is " + familiesMoreThan);
                break;
            case REMOVE_ALL_CHILDREN_OLDER_THAN:
                ConsoleOUT.number();
                int age = 0;
                try {
                    age = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    mainMenu();
                }
                boolean deleted = controller.deleteAllChildrenOlderThen(age);
                if (deleted) {
                    ConsoleOUT.childrenDeleted();
                    mainMenu();
                } else {
                    ConsoleOUT.childrenDidntDeleted();
                    mainMenu();
                }
            case EXIT:
                break;
            default:
                ConsoleOUT.wrongCommand();
                mainMenu();
                break;
        }
    }

    private void deleteFamily() throws IOException {
        ConsoleOUT.familyIDX();
        int familyIndex = 0;
        try {
            familyIndex = Integer.parseInt(ConsoleIN.reader());
        } catch (Exception e) {
            ConsoleOUT.wrongIdx();
            mainMenu();
        }
        boolean deleted = controller.deleteFamily(familyIndex);
        if (deleted) {
            ConsoleOUT.familyDeleted();
            mainMenu();
        } else {
            ConsoleOUT.doesntExist();
            mainMenu();
        }
    }

    private void createFamily() throws IOException {
        ConsoleOUT.motherName();
        String motherName = ConsoleIN.reader();
        ConsoleOUT.motherSurname();
        String motherSurname = ConsoleIN.reader();
        int motherYear = 0;
        int motherMonth = 0;
        int motherDay = 0;
        int motherIQ = 0;
        try {
            ConsoleOUT.year();
            motherYear = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.month();
            motherMonth = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.day();
            motherDay = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.motherIQ();
            motherIQ = Integer.parseInt(ConsoleIN.reader());
        } catch (Exception e) {
            ConsoleOUT.wrongCommand();
            mainMenu();
        }
        ConsoleOUT.scheduleAdd();
        ConsoleOUT.displayScheduleMenu();
        String answerMother = ConsoleIN.reader();
        Map<String, String> motherActivities = new HashMap<>();
        switch (answerMother){
            case "1":
                motherActivities=addingSchedule();
                break;
            case "2":
                break;
            default:
                ConsoleOUT.wrongCommand();
                mainMenu();
        }
        ConsoleOUT.fatherName();
        String fatherName = ConsoleIN.reader();
        ConsoleOUT.fatherSurname();
        String fatherSurname = ConsoleIN.reader();
        int fatherYear = 0;
        int fatherMonth = 0;
        int fatherDay = 0;
        int fatherIQ = 0;
        try {
            ConsoleOUT.year();
            fatherYear = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.month();
            fatherMonth = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.day();
            fatherDay = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.fatherIQ();
            fatherIQ = Integer.parseInt(ConsoleIN.reader());
        } catch (Exception e) {
            ConsoleOUT.wrongCommand();
            mainMenu();
        }
        ConsoleOUT.scheduleAdd();
        ConsoleOUT.displayScheduleMenu();
        String answerFather = ConsoleIN.reader();
        Map<String, String> fatherActivities = new HashMap<>();
        switch (answerFather){
            case "1":
                motherActivities=addingSchedule();
                break;
            case "2":
                break;
            default:
                ConsoleOUT.wrongCommand();
                mainMenu();
        }
        Human mother = null;
        Human father = null;
        try {
            mother = new Woman(motherName, motherSurname, motherYear, motherMonth, motherDay, motherIQ);
            mother.setSchedule(motherActivities);
            father = new Man(fatherName, fatherSurname, fatherYear, fatherMonth, fatherDay, fatherIQ);
            father.setSchedule(fatherActivities);
        } catch (Exception e) {
            ConsoleOUT.wrongCommand();
            mainMenu();
        }
        Family newFamily = controller.createNewFamily(mother, father);
        ConsoleOUT.familyCreated();
        ConsoleOUT.print(newFamily.prettyFormat());
    }

    private static Map<String, String> addingSchedule() throws IOException {
        schedule = new HashMap<>();
        ConsoleOUT.countOfActivities();
        int count = 0;
        try {
            count = Integer.parseInt(ConsoleIN.reader());
        } catch (Exception e) {
            ConsoleOUT.wrongCommand();
            addingSchedule();
        }
        for (int i = 0; i < count; i++) {
            ConsoleOUT.dayOfWeek();
            String dayOfWeek = ConsoleIN.reader();
            ConsoleOUT.activity();
            String activity = ConsoleIN.reader();
            schedule.put(dayOfWeek, activity);
        }
        return schedule;
    }

    private void editFamily(int index) throws IOException {
        ConsoleOUT.displayChildMenu();
        String command = ConsoleIN.reader();
        int size = 0;
        if (controller.getFamilyByIndex(index).getChildrens()!=null){
            size = controller.getFamilyByIndex(index).getChildrens().size();
        }
        switch (command) {
            case GIVE_BIRTH_TO_BABY:
                if (size <= 5) {
                    childBirth(index);
                } else {
                    throw new FamilyOverflowException("Too many children for one family");
                }
                break;
            case ADOPT_CHILD:
                if (size <= 5) {
                    childBirth(index);
                } else {
                    throw new FamilyOverflowException("No way too adopt child, too many children");
                }
                break;
            case ADD_PETS:
                addPets(index);
                break;
            case RETURN_TO_MAIN_MENU:
                mainMenu();
            default:
                ConsoleOUT.wrongCommand();
                mainMenu();
        }
    }

    private void childBirth(int index) throws IOException {
        Human child = null;
        ConsoleOUT.childName();
        String name = ConsoleIN.reader();
        String surname = controller.getFamilyByIndex(index).getFather().getSurname();
        int childYear = 0;
        int childMonth = 0;
        int childDay = 0;
        try {
            ConsoleOUT.year();
            childYear = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.month();
            childMonth = Integer.parseInt(ConsoleIN.reader());
            ConsoleOUT.day();
            childDay = Integer.parseInt(ConsoleIN.reader());
        } catch (Exception e) {
            ConsoleOUT.wrongCommand();
            childBirth(index);
        }
        ConsoleOUT.gender();
        String gender = ConsoleIN.reader();
        if (gender.equals("1")) {
            try {
                child = new Man(name, surname, childYear, childMonth, childDay, 0);
            } catch (Exception e) {
                ConsoleOUT.wrongCommand();
                childBirth(index);
            }
        } else if (gender.equals("2")) {
            try {
                child = new Woman(name, surname, childYear, childMonth, childDay, 0);
            } catch (Exception e) {
                ConsoleOUT.wrongCommand();
                childBirth(index);
            }
        } else {
            ConsoleOUT.wrongCommand();
            childBirth(index);
        }
        controller.bornChild(controller.getFamilyByIndex(index), child);
        ConsoleOUT.childCreated();
        mainMenu();
    }

    private void addPets(int familyIndex) throws IOException {
        ConsoleOUT.countOfPets();
        int petCount = 0;
        try {
            petCount = Integer.parseInt(ConsoleIN.reader());
        } catch (Exception e) {
            ConsoleOUT.wrongCommand();
            addPets(familyIndex);
        }
        ConsoleOUT.choosingPetSpecies();
        String specie = ConsoleIN.reader();
        switch (specie) {
            case DOG:
                ConsoleOUT.petName();
                String dogName = ConsoleIN.reader();
                ConsoleOUT.petAge();
                int dogAge = 0;
                try {
                    dogAge = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    addPets(familyIndex);
                }
                Pet dog = new Dog(dogName, dogAge);
                petAmount(dog, petCount, familyIndex);
                ConsoleOUT.petAdded();
                mainMenu();
                break;
            case FISH:
                ConsoleOUT.petName();
                String fishName = ConsoleIN.reader();
                ConsoleOUT.petAge();
                int fishAge = 0;
                try {
                    fishAge = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    addPets(familyIndex);
                }
                Pet fish = new Fish(fishName, fishAge);
                petAmount(fish, petCount, familyIndex);
                ConsoleOUT.petAdded();
                mainMenu();
                break;
            case CAT:
                ConsoleOUT.petName();
                String catName = ConsoleIN.reader();
                ConsoleOUT.petAge();
                int catAge = 0;
                try {
                    catAge = Integer.parseInt(ConsoleIN.reader());
                } catch (Exception e) {
                    ConsoleOUT.wrongCommand();
                    addPets(familyIndex);
                }
                Pet cat = new Cat(catName, catAge);
                petAmount(cat, petCount, familyIndex);
                ConsoleOUT.petAdded();
                mainMenu();
                break;
            default:
                ConsoleOUT.wrongCommand();
                addPets(familyIndex);
        }
    }

    private void petAmount(Pet pet, int count, int familyIndex) {
        for (int i = 0; i < count; i++) {
            controller.addPet(familyIndex, pet);
        }
    }
}
