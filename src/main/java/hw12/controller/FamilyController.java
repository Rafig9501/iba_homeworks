package hw12.controller;

import hw12.entity.Family;
import hw12.entity.Human;
import hw12.entity.Pet;
import hw12.service.FamilyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FamilyController {

    static FamilyService familyService;
    static List<Family> families;

    public FamilyController() {
        if (familyService == null)
            familyService = new FamilyService();
        if (families == null)
            families = new ArrayList<>();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public int getFamiliesBiggerThan(int peopleAmount) {
        return familyService.getFamiliesBiggerThan(peopleAmount);
    }

    public int getFamiliesLessThan(int peopleAmount) {
        return familyService.getFamiliesLessThan(peopleAmount);
    }

    public int countFamiliesWithMemberNumber(int peopleAmount) {
        return familyService.countFamiliesWithMemberNumber(peopleAmount);
    }

    public Family createNewFamily(Human mother, Human father) {
        return familyService.createNewFamily(mother, father);
    }

    public Family bornChild(Family family, Human human) {
        return familyService.bornChild(family, human);
    }

    public Family adoptChild(Family family, Human human) {
        return familyService.adoptChild(family, human);
    }

    public boolean deleteAllChildrenOlderThen(int age) {
        return familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Optional<Family> getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public List<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public List<Pet> addPet(int index, Pet pet) {
        return familyService.addPet(index, pet);
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        if (getFamilyById(index).isPresent()) {
            return familyService.deleteFamily(index);
        }else return false;
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }
}
