package hw12.entity;

import hw12.utilities.Species;

public class Dog extends Pet {

    Species species;

    public Dog(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
        this.species = Species.DOG;
    }

    public Dog(String nickname, int age) {
        super(nickname, age);
        this.species = Species.DOG;
    }
}
