package hw12.entity;

public class Cat extends Pet {

    public Cat(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
    }

    public Cat(String nickname, int age) {
        super(nickname, age);
    }
}
