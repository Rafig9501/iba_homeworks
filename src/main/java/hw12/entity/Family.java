package hw12.entity;

import java.util.List;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;
    private List<Human> childrens;
    private List<Pet> pets;
    private int id;
    private static int count;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        id = ++count;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(List<Human> childrens) {
        this.childrens = childrens;
    }

    public void setChildrens(List<Human> childrens) {
        this.childrens = childrens;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setPets(List<Pet> pets) {
        this.pets = pets;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildrens() {
        return childrens;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return id == family.id &&
                Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(childrens, family.childrens) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, childrens, pets, id);
    }

    @Override
    public String toString() {
        return prettyFormat();
    }

    public String prettyFormat() {
        return "Family{" +
                "\nmother : " + mother +
                ", \nfather : " + father +
                ", \nchildrens : " + childrens +
                ", \npets : " + pets +
                ", \nid : " + id +
                '}';
    }
}
