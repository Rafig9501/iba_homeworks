package hw12.entity;


import java.text.ParseException;

public final class Man extends Human {

    public Man(String name, String surname, int yyyy, int MM, int dd,int IQ) throws ParseException {
        super(name, surname, yyyy, MM, dd, IQ);
    }

    public Man(String name) {
        super(name);
    }

    public Man() {
        super();
    }
}
