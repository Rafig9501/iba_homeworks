package hw12.entity;

import java.text.ParseException;

public final class Woman extends Human {

    public Woman(String name, String surname, int yyyy, int MM, int dd,int IQ) throws ParseException {
        super(name, surname, yyyy, MM, dd, IQ);
    }

    public Woman(String name) {
        super(name);
    }

    public Woman() {
        super();
    }

}
