package hw12.console;

import hw12.utilities.Species;

public class ConsoleOUT {

    public static void print(String string) {
        System.out.println(string);
    }

    public static void displayMainMenu() {
        print("==========================================");
        print("            HAPPY FAMILY APP          ");
        print("==========================================");
        print("");
        print("   1. Create Family ");
        print("   2. Delete Family ");
        print("   3. Edit Family ");
        print("   4. Get All Families ");
        print("   5. Families with count of people less than :  ");
        print("   6. Families with count of people more than :  ");
        print("   7. Families with count of people equals to :  ");
        print("   8. Remove all children older than :  ");
        print("   9. Exit ");
        print("==========================================");
        print("");
        print("==========================================");
    }

    public static void displayChildMenu() {
        print("==========================================");
        print("            HAPPY FAMILY APP          ");
        print("==========================================");
        print("");
        print("   1. Give birth to a baby ");
        print("   2. Adopt a child ");
        print("   3. Add pets");
        print("   4. Return to main menu ");
        print("==========================================");
        print("");
        print("==========================================");
    }

    public static void choosingPetSpecies(){
        print("   1." + Species.DOG);
        print("   2." + Species.FISH);
        print("   3." + Species.CAT);
    }

    public static void displayScheduleMenu() {
        print("   1. Yes");
        print("   2. No");
    }

    public static void number() {
        print("Please enter number : ");
    }

    public static void motherName() {
        print("Please enter mother's name");
    }

    public static void motherSurname() {
        print("Please enter mother's surname");
    }

    public static void year() {
        print("Please enter year");
    }

    public static void month() {
        print("Please enter month");
    }

    public static void day() {
        print("Please enter day");
    }

    public static void motherIQ() {
        print("Please enter mother's IQ");
    }

    public static void fatherName() {
        print("Please enter father's name");
    }

    public static void fatherSurname() {
        print("Please enter father's surname");
    }

    public static void fatherIQ() {
        print("Please enter father IQ");
    }

    public static void familyIDX() {
        print("Please enter family index");
    }

    public static void childName() {
        print("Please enter child's name which you would like to give to child");
    }

    public static void gender(){ print("Please enter 1 for Male, 2 for Female");}

    public static void age() {
        print("Please enter the age");
    }

    public static void wrongCommand() {
        print("Please enter right command");
    }

    public static void familyCreated() {
        print("Family has been created");
    }

    public static void scheduleAdd() {
        print("Would you like to add schedules?");
    }

    public static void dayOfWeek() {
        print("Please enter day of week");
    }

    public static void activity() {
        print("Please enter activity");
    }

    public static void countOfActivities() {
        print("How many activities you would like to add?");
    }

    public static void countOfPets(){ print("How many pets would you like to add?");}

    public static void petName(){ print("Please enter pet name");}

    public static void petAge(){print("Please enter pet age");}

    public static void wrongIdx(){print("Please enter right family index");}

    public static void familyDeleted(){print("This family has been deleted");}

    public static void childCreated() {print("child has been added");}

    public static void doesntExist(){print("This family doesn't exist");}

    public static void petAdded() {
        print("Pet added");
    }

    public static void childrenDeleted() {
        print("Children older than this age has been deleted");
    }

    public static void childrenDidntDeleted(){
        print("There is no children older than this age");
    }
}
