package hw09.utilities;

public enum Species {

    DOG, CAT, FISH, UNKNOWN;
}
