package hw09.entity;

public final class Woman extends Human {

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name) {
        super(name);
    }

    public Woman() {
        super();
    }


}
