package hw09.entity;

import java.util.List;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;
    private List<Human> childrens;
    private Pet pet;
    private List<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(List<Human> childrens) {
        this.childrens = childrens;
    }

    public void setChildrens(List<Human> childrens) {
        this.childrens = childrens;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setPets(List<Pet> pets) {
        this.pets = pets;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Pet getPet() {
        return pet;
    }

    public List<Human> getChildrens() {
        return childrens;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Family family = (Family) object;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(childrens, family.childrens) &&
                Objects.equals(pet, family.pet) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, childrens, pet, pets);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", childrens=" + childrens +
                ", pet=" + pet +
                ", pets=" + pets +
                '}';
    }
}
