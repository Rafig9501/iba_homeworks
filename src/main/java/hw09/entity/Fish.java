package hw09.entity;

import hw09.utilities.Species;

public class Fish extends Pet {

    Species species;

    public Fish(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
        this.species = Species.FISH;
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
        this.species = Species.FISH;
    }
}
