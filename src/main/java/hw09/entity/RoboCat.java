package hw09.entity;

public class RoboCat extends Pet {

    public RoboCat(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
    }

    public RoboCat(String nickname, int age) {
        super(nickname, age);
    }
}
