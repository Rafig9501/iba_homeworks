package hw09.entity;


public final class Man extends Human{

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name) {
        super(name);
    }

    public Man() {
        super();
    }
}
