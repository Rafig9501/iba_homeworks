package hw11.entity;


public final class Man extends Human {

    public Man(String name, String surname, int yyyy, int MM, int dd) {
        super(name, surname, yyyy, MM, dd);
    }

    public Man(String name) {
        super(name);
    }

    public Man() {
        super();
    }
}
