package hw11.entity;

public final class Woman extends Human {

    public Woman(String name, String surname, int yyyy, int MM, int dd) {
        super(name, surname, yyyy, MM, dd);
    }

    public Woman(String name) {
        super(name);
    }

    public Woman() {
        super();
    }

}
