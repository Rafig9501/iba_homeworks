package hw11.entity;

import hw11.utilities.Species;

public class DomesticCat extends Pet {

    Species species;

    public DomesticCat(String nickname, int age,String[] habits) {
        super(nickname, age, habits);
        this.species = Species.CAT;
    }

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
        this.species = Species.DOG;
    }
}
