package hw11.service;

import hw11.dao.CollectionFamilyDao;
import hw11.dao.FamilyDAO;
import hw11.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService implements CollectionFamilyDao {

    static FamilyDAO familyService;
    static List<Family> families;

    public FamilyService() {
        familyService = this;
        if (families == null) {
            families = new ArrayList<>();
        }
    }

    @Override
    public void displayAllFamilies() {
        System.out.println(families.toString());
    }

    @Override
    public int getFamiliesBiggerThan(int peopleAmount) {
        return (int) families.stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) > peopleAmount).count();
    }

    @Override
    public int getFamiliesLessThan(int peopleAmount) {
        return (int) families.stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) < peopleAmount).count();
    }

    @Override
    public int countFamiliesWithMemberNumber(int peopleAmount) {
        return (int) families.stream().filter(family
                -> (family.getChildrens() != null ? family.getChildrens().size() + 2 : 2) == peopleAmount).count();
    }

    @Override
    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        families.add(family);
        return family;
    }

    @Override
    public Family bornChild(Family family, Human human, String manName, String womanName) {
        if (human.getClass() == Woman.class) human.setName(womanName);
        if (human.getClass() == Man.class) human.setName(manName);
        if (family.getChildrens() != null) {
            family.getChildrens().add(human);
        }
        if (family.getChildrens() == null) {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(human);
        }
        saveFamily(family);
        return family;
    }

    @Override
    public Family adoptChild(Family family, Human human) {
        if (family.getChildrens() != null) {
            family.getChildrens().add(human);
        }
        if (family.getChildrens() == null) {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(human);
        }
        return family;
    }

    @Override
    public List<Family> deleteAllChildrenOlderThen(int age) {
        return families.stream().filter(family -> {
            List<Human> collect = family.getChildrens().stream().filter(h -> h.getAgeInYears() < age).collect(Collectors.toList());
            return true;
        }).collect(Collectors.toList());
    }

    @Override
    public int count() {
        return families.size();
    }

    @Override
    public Family getFamilyById(int index) {
        if (index < families.size()) {
            return families.get(index);
        } else return getFamilyById(families.size() - 1);
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        return families.get(familyIndex).getPets();
    }

    @Override
    public List<Pet> addPet(int familyIndex, Pet pet) {
        if (families == null || families.get(familyIndex) == null) {
            return null;
        } else if (families.get(familyIndex).getPets() == null) {
            families.get(familyIndex).setPets(new ArrayList<>());
            families.get(familyIndex).getPets().add(pet);
            return families.get(familyIndex).getPets();
        } else {
            families.get(familyIndex).getPets().add(pet);
            return families.get(familyIndex).getPets();
        }
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (families.size() > index && families.get(index) != null) {
            families.remove(index);
            return true;
        } else return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (families.contains(family)) {
            families.remove(family);
            return true;
        } else return false;
    }

    @Override
    public void saveFamily(Family family) {
        families.add(family);
    }
}
