package hw11.utilities;

public enum Species {

    DOG, CAT, FISH, UNKNOWN
}
